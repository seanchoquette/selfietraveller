package com.selfie.traveller.application.dagger.modules;


import android.content.Context;

/**/
import com.selfie.traveller.application.dagger.AppScope;
import com.selfie.traveller.ext.storage.PreferencesManager;

import dagger.Module;
import dagger.Provides;

@Module
public class StorageModule {

    @AppScope
    @Provides
    public PreferencesManager preferencesManager(Context context)
    {
        return new PreferencesManager(context);
    }
}
