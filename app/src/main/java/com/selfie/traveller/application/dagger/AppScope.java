package com.selfie.traveller.application.dagger;


import javax.inject.Scope;

@Scope
public @interface AppScope {
}
