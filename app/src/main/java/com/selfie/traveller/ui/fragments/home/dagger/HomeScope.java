package com.selfie.traveller.ui.fragments.home.dagger;


import javax.inject.Scope;

@Scope
public @interface HomeScope {
}
