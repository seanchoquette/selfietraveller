package com.selfie.traveller.ui.activities.register;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

@AutoValue
public abstract class RegisterParams implements Parcelable {


    @SerializedName("first_name")
    public abstract String firstName();

    @SerializedName("last_name")
    public abstract String lastName();

    @SerializedName("email")
    public abstract String email();

    @SerializedName("password")
    public abstract String password();

    @SerializedName("password_confirmation")
    public abstract String passwordConfirmation();

    @SerializedName("phone")
    public abstract String phone();

    @SerializedName("address")
    public abstract String address();

    @SerializedName("city")
    public abstract String city();

    @SerializedName("state")
    public abstract String state();

    @SerializedName("zip")
    public abstract String zip();

    @SerializedName("country")
    public abstract String country();

    @SerializedName("cardnumber")
    public abstract String cardnumber();

    @SerializedName("nameoncard")
    public abstract String nameoncard();

    @SerializedName("ccv")
    public abstract String ccv();

    @SerializedName("expdate")
    public abstract String expdate();


    @AutoValue.Builder
    public abstract static class Builder {

        public abstract Builder firstName(String firstName);

        public abstract Builder lastName(String lastName);


        public abstract Builder email(String email);


        public abstract Builder password(String password);

        public abstract Builder passwordConfirmation(String passwordConfirmation);

        public abstract Builder phone(String phone);

        public abstract Builder address(String address);

        public abstract Builder city(String city);

        public abstract Builder state(String state);

        public abstract Builder zip(String zip);

        public abstract Builder country(String country);

        public abstract Builder cardnumber(String cardnumber);

        public abstract Builder nameoncard(String nameoncard);

        public abstract Builder ccv(String ccv);

        public abstract Builder expdate(String expdate);


        public abstract RegisterParams build();
    }

    public static Builder builder() {
        return new $$AutoValue_RegisterParams.Builder();
    }


    public static TypeAdapter<RegisterParams> typeAdapter(Gson gson) {
        return new $AutoValue_RegisterParams.GsonTypeAdapter(gson);
    }

}
