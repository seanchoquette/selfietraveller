package com.selfie.traveller.ui.activities.register.dialog;


import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.widget.Toast;

import com.selfie.traveller.R;
import com.selfie.traveller.databinding.RegisterLayoutFifthBinding;
import com.selfie.traveller.ext.AppUtils;

import java.io.File;
import java.io.IOException;

public class RegisterFifthDialog {


    Dialog dialog;
    Activity activity;
    String[] PERMISSIONS;
    File tempOutputFile;
    public static final int REQUEST_TAKE_PHOTO1 = 1;
    public static int RESULT_LOAD_IMAGE1 = 1;

    public RegisterLayoutFifthBinding binding;


    public RegisterFifthDialog(Activity activity) {
        this.activity = activity;
        dialog = new Dialog(activity, R.style.DialogStyle);
        binding = DataBindingUtil.inflate(LayoutInflater.from(activity),R.layout.register_layout_fifth,null,false);
        dialog.setContentView(binding.getRoot());



        binding.ivCamera.setOnClickListener(view ->
        {

            PERMISSIONS = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
            ActivityCompat.requestPermissions(activity, PERMISSIONS, 1);
            if(AppUtils.hasPermissions(activity, PERMISSIONS)){
                TakePhotoGallery(RESULT_LOAD_IMAGE1,REQUEST_TAKE_PHOTO1);
            }else{
                Toast.makeText(activity, "You have to turn on camera permission mannualy.", Toast.LENGTH_SHORT).show();
            }

        });

        binding.btnRedo.setOnClickListener(view ->
        {
            PERMISSIONS = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
            ActivityCompat.requestPermissions(activity, PERMISSIONS, 1);
            if(AppUtils.hasPermissions(activity, PERMISSIONS)){
                TakePhotoGallery(RESULT_LOAD_IMAGE1,REQUEST_TAKE_PHOTO1);
            }else{
                Toast.makeText(activity, "You have to turn on camera permission mannualy.", Toast.LENGTH_SHORT).show();
            }

        });
    }


    //gallery or take photo
    public void TakePhotoGallery(final int requestcodegallery, final int requestcodephoto) {

        Intent selectImageIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        selectImageIntent.setType("image/*");
        selectImageIntent.putExtra("crop", "true");
        selectImageIntent.putExtra("scale", true);
        selectImageIntent.putExtra("outputX", 256);
        selectImageIntent.putExtra("outputY", 256);
        selectImageIntent.putExtra("aspectX", 1);
        selectImageIntent.putExtra("aspectY", 1);
        selectImageIntent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        selectImageIntent.putExtra(MediaStore.EXTRA_OUTPUT, getTempUri());
        activity.startActivityForResult(selectImageIntent, requestcodegallery);

        /*List<String> option = Arrays.asList(activity.getResources().getStringArray(R.array.option));
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,
                android.R.layout.select_dialog_item, option);
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.DialogStyle);

        builder.setTitle(activity.getString(R.string.select_option));
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                Log.e("Selected Item", String.valueOf(which));

                if (which == 1) {

                    Intent selectImageIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                    selectImageIntent.setType("image/*");
                    selectImageIntent.putExtra("crop", "true");
                    selectImageIntent.putExtra("scale", true);
                    selectImageIntent.putExtra("outputX", 256);
                    selectImageIntent.putExtra("outputY", 256);
                    selectImageIntent.putExtra("aspectX", 1);
                    selectImageIntent.putExtra("aspectY", 1);
                    selectImageIntent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
                    selectImageIntent.putExtra(MediaStore.EXTRA_OUTPUT, getTempUri());
                    activity.startActivityForResult(selectImageIntent, requestcodegallery);
                }

            }
        });
        builder.show();*/

    }



    private Uri getTempUri() {
        return Uri.fromFile(getTempFile());
    }

    private File getTempFile() {

        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {

            File file = new File(Environment.getExternalStorageDirectory(), "TEMP_PHOTO_FILE");
            try {
                file.createNewFile();
            } catch (IOException e) {

            }

            return file;
        } else {

            return null;
        }
    }

    public void showDialog(boolean doShow) {
        if (doShow){
           // dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.show();
        }
        else
            dialog.dismiss();
    }


}
