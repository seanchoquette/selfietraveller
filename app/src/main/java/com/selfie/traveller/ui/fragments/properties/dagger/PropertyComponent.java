package com.selfie.traveller.ui.fragments.properties.dagger;


import com.selfie.traveller.application.dagger.AppComponent;
import com.selfie.traveller.ui.fragments.properties.PropertyListFragment;

import dagger.Component;

@PropertyScope
@Component(modules = PropertyModule.class, dependencies = AppComponent.class)
public interface PropertyComponent {

    void inject(PropertyListFragment fragment);
}
