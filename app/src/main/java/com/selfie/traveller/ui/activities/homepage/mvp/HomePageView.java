package com.selfie.traveller.ui.activities.homepage.mvp;

import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;

import com.selfie.traveller.R;
import com.selfie.traveller.ext.storage.PreferencesManager;
import com.selfie.traveller.ui.fragments.home.HomeFragment;
import com.selfie.traveller.ui.fragments.properties.PropertyListFragment;

import butterknife.BindView;
import butterknife.ButterKnife;


public class HomePageView extends FrameLayout {

    public int selectedItemPosition = 0;

    public static String currentFragment = "";
    public PreferencesManager preferencesManager;
    public @BindView(R.id.tabs)
    TabLayout allTabs;

    int currentPosition;
    HomeFragment homeFragment;
    PropertyListFragment propertyListFragment;

    FragmentManager fragmentManager;
    public AppCompatActivity activity;
    FragmentTransaction fragmentTransaction;

    public HomePageView(@NonNull AppCompatActivity appCompatActivity,
                        HomeFragment homeFragment,
PropertyListFragment propertyListFragment,
                        PreferencesManager preferencesManager) {
        super(appCompatActivity);
        this.activity = appCompatActivity;
        this.homeFragment = homeFragment;
this.propertyListFragment= propertyListFragment;
        inflate(activity, R.layout.activity_homepage, this);
        this.preferencesManager = preferencesManager;
        ButterKnife.bind(this);

        setupTabLayout();
        setCurrentTabFragment(0);

    }




    public void loadFragment(Fragment fragment, String tag) {
        currentFragment = tag;
        preferencesManager.save("CurrentFragment",tag);
        fragmentManager = activity.getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        //fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        fragmentTransaction.replace(R.id.container, fragment, tag);
        //fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commitAllowingStateLoss();
    }




    private void setupTabLayout() {
        View view1g = activity.getLayoutInflater().inflate(R.layout.tab_property, null);
        view1g.findViewById(R.id.icon).setBackgroundResource(R.drawable.ic_home);
        allTabs.addTab(allTabs.newTab().setCustomView(view1g).setIcon(R.drawable.ic_home));

        View view2g = activity.getLayoutInflater().inflate(R.layout.tab_message, null);
        view2g.findViewById(R.id.icon).setBackgroundResource(R.drawable.ic_message);
        allTabs.addTab(allTabs.newTab().setCustomView(view2g).setIcon(R.drawable.ic_message));

        View view3g = activity.getLayoutInflater().inflate(R.layout.tab_home, null);
        view3g.findViewById(R.id.icon).setBackgroundResource(R.drawable.ic_logo);
        allTabs.addTab(allTabs.newTab().setCustomView(view3g).setIcon(R.drawable.ic_logo));

        View view4g = activity.getLayoutInflater().inflate(R.layout.tab_profile, null);
        view4g.findViewById(R.id.icon).setBackgroundResource(R.drawable.ic_profile);
        allTabs.addTab(allTabs.newTab().setCustomView(view4g).setIcon(R.drawable.ic_profile));

        View view5g = activity.getLayoutInflater().inflate(R.layout.tab_setting, null);
        view5g.findViewById(R.id.icon).setBackgroundResource(R.drawable.ic_setting);
        allTabs.addTab(allTabs.newTab().setCustomView(view5g).setIcon(R.drawable.ic_setting));
    }


    public void setCurrentTabFragment(int tabPosition) {
        switch (tabPosition) {
            case 0:
               loadFragment(propertyListFragment,"property");
                currentPosition = 0;
                break;
            case 1:

                currentPosition = 1;
                break;
            case 2:
                loadFragment(homeFragment,"home");
                currentPosition = 2;

                break;
            case 3:
                currentPosition = 3;
                break;
            case 4:
                currentPosition = 4;

                break;
        }
    }


}
