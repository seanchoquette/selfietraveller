package com.selfie.traveller.ui.fragments.properties.adapter;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import com.selfie.traveller.databinding.PropertyListItemBinding;
import com.selfie.traveller.ui.fragments.properties.propertiesResponse.Datum;

import java.util.ArrayList;
import java.util.List;


public class PropertyAdapterN extends RecyclerView.Adapter<PropertyViewHolder> {


    Activity activity;
    //private PublishSubject<DatumN> clickDetail = PublishSubject.create();
    private List<Datum> arrayList = new ArrayList<>();

    @Override
    public PropertyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        PropertyViewHolder viewHolder = new PropertyViewHolder(PropertyListItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));


       /* RxView.clicks(viewHolder.binding.rl1)
                .takeUntil(RxView.detaches(parent))
                .map(aVoid -> arrayList.get(viewHolder.getAdapterPosition()))
                .subscribe(clickDetail);*/

        return viewHolder;
    }



    @Override
    public void onBindViewHolder(PropertyViewHolder holder, int position) {
       try {
           holder.binding.tvBath.setText(arrayList.get(position).getBaths()+"");
           holder.binding.tvPrice.setText(arrayList.get(position).getPrice()+"");
           holder.binding.tvQuest.setText(arrayList.get(position).getGuests()+"");
           holder.binding.tvRooms.setText(arrayList.get(position).getRooms()+"");
           holder.binding.tvTitle.setText(arrayList.get(position).getTitle()+"");
           //Picasso.with(activity).load(arrayList.get(position).getPhotos()).into(holder.binding.ivImage);
       }catch (Exception e){
           Toast.makeText(activity, e.toString(), Toast.LENGTH_SHORT).show();
       }


    }

    public void showList(List<Datum> response, AppCompatActivity activity) {
        this.arrayList.clear();
        this.activity = activity;
        if (response != null && !response.isEmpty())
            this.arrayList.addAll(response);
        notifyDataSetChanged();
    }


    /*public Observable<Datum> getDetailClickedObservable() {
        return clickDetail;
    }*/

    @Override
    public int getItemCount() {
        return arrayList.size();

    }


}
