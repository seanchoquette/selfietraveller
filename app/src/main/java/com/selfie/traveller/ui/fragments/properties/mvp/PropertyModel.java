package com.selfie.traveller.ui.fragments.properties.mvp;

import android.support.v7.app.AppCompatActivity;


import com.selfie.traveller.application.AppNetwork;
import com.selfie.traveller.ext.CommonResponse.CommonResponse;
import com.selfie.traveller.ext.storage.PreferencesManager;
import com.selfie.traveller.ui.fragments.properties.AddpropertyParams;
import com.selfie.traveller.ui.fragments.properties.propertiesResponse.PropertyResponse;
import com.selfie.traveller.utils.Constants;

import io.reactivex.Observable;


public class PropertyModel {

    private final AppNetwork appNetwork;
    private AppCompatActivity activity;
    PreferencesManager preferencesManager;


    public PropertyModel(AppCompatActivity activity, AppNetwork appNetwork, PreferencesManager preferencesManager) {
        this.activity = activity;
        this.appNetwork = appNetwork;
        this.preferencesManager = preferencesManager;

    }

    public Observable<PropertyResponse> getPropertyObservable(int page) {

        String url =Constants.PROPERTYLIST+"?page="+page;
        return appNetwork.getAllProperty(url,preferencesManager.get(Constants.TOKEN));
    }

    public Observable<CommonResponse> addPropertyObservable(AddpropertyParams addpropertyParams) {

        return appNetwork.addProperty(preferencesManager.get(Constants.TOKEN),addpropertyParams);
    }

    public String getData(String key) {
        return preferencesManager.get(key);
    }

    public void saveData(String key, String value) {
        preferencesManager.save(key, value);
    }

}


