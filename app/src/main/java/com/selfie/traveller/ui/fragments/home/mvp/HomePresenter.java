package com.selfie.traveller.ui.fragments.home.mvp;



public class HomePresenter {

    private final HomeView homeView;
    private final HomeModel homeModel;


    public HomePresenter(HomeView homeView, HomeModel homeModel) {
        this.homeView = homeView;
        this.homeModel = homeModel;
    }


    public void onCreateViewResume() {

    }


    public void onCreateView() {


    }

}
