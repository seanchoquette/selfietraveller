package com.selfie.traveller.ui.fragments.properties.dagger;


import android.support.v7.app.AppCompatActivity;


import com.selfie.traveller.application.AppNetwork;
import com.selfie.traveller.ext.storage.PreferencesManager;
import com.selfie.traveller.ui.fragments.properties.adapter.PropertyAdapterN;
import com.selfie.traveller.ui.fragments.properties.dialog.AddPropertyDialog;
import com.selfie.traveller.ui.fragments.properties.mvp.PropertyModel;
import com.selfie.traveller.ui.fragments.properties.mvp.PropertyPresenter;
import com.selfie.traveller.ui.fragments.properties.mvp.PropertyView;

import dagger.Module;
import dagger.Provides;

@Module
public class PropertyModule {

    private final AppCompatActivity activity;

    public PropertyModule(AppCompatActivity activity) {
        this.activity = activity;
    }

    @PropertyScope
    @Provides
    public PropertyView view(PreferencesManager preferencesManager, PropertyAdapterN adapterN,AddPropertyDialog addPropertyDialog) {
        return new PropertyView(activity, preferencesManager,adapterN,addPropertyDialog);
    }

    @PropertyScope
    @Provides
    public PropertyModel newsModel(AppNetwork network, PreferencesManager preferencesManager) {
        return new PropertyModel(activity, network, preferencesManager);
    }

    @PropertyScope
    @Provides
    public PropertyPresenter presenter(PropertyView view, PropertyModel model) {
        return new PropertyPresenter(view, model);
    }

    @PropertyScope
    @Provides
    public PropertyAdapterN adapterN() {
        return new PropertyAdapterN();
    }


    @PropertyScope
    @Provides
    public AddPropertyDialog addPropertyDialog() {
        return new AddPropertyDialog(activity);
    }


}
