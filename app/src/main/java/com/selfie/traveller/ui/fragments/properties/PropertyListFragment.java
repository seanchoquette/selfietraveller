package com.selfie.traveller.ui.fragments.properties;

import android.Manifest;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.selfie.traveller.BuildConfig;
import com.selfie.traveller.application.dagger.AppApplication;
import com.selfie.traveller.ext.AppUtils;
import com.selfie.traveller.ui.fragments.properties.dagger.DaggerPropertyComponent;
import com.selfie.traveller.ui.fragments.properties.dagger.PropertyModule;
import com.selfie.traveller.ui.fragments.properties.mvp.PropertyPresenter;
import com.selfie.traveller.ui.fragments.properties.mvp.PropertyView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import id.zelory.compressor.Compressor;


public class PropertyListFragment extends Fragment {

    String[] PERMISSIONS;
    public  File tempOutputFile;
    public static final int REQUEST_TAKE_PHOTO1 = 1;
    public static int RESULT_LOAD_IMAGE1 = 1;
    File compressedImage;
    private File actualImage;
    File pictureFile;
    String picturePath1, picturePathG;


    @Inject
    PropertyView view;

    @Inject
    PropertyPresenter presenter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        DaggerPropertyComponent.builder()
                .appComponent(AppApplication.get(getActivity()).appComponent())
                .propertyModule(new PropertyModule((AppCompatActivity)getActivity()))
                .build()
                .inject(this);
        presenter.onCreateView();

       view.addPropertyDialog.binding.ivProfile.setOnClickListener(view ->
        {

            PERMISSIONS = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
            ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, 1);
            if(AppUtils.hasPermissions(getActivity(), PERMISSIONS)){
                //TakePhotoGallery(RESULT_LOAD_IMAGE1,REQUEST_TAKE_PHOTO1);
                dispatchTakePictureIntent(RESULT_LOAD_IMAGE1);
            }else{
                Toast.makeText(getActivity(), "You have to turn on camera permission mannualy.", Toast.LENGTH_SHORT).show();
            }

        });
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroyView();

    }
    @Override
    public void onResume() {
        super.onResume();


    }



    /*    //gallery or take photo
        public void TakePhotoGallery(final int requestcodegallery, final int requestcodephoto) {

            Intent selectImageIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
            selectImageIntent.setType("image/*");
            selectImageIntent.putExtra("crop", "true");
            selectImageIntent.putExtra("scale", true);
            selectImageIntent.putExtra("outputX", 256);
            selectImageIntent.putExtra("outputY", 256);
            selectImageIntent.putExtra("aspectX", 1);
            selectImageIntent.putExtra("aspectY", 1);
            selectImageIntent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
            selectImageIntent.putExtra(MediaStore.EXTRA_OUTPUT, getTempUri());
            activity.startActivityForResult(selectImageIntent, requestcodegallery);

            *//*List<String> option = Arrays.asList(activity.getResources().getStringArray(R.array.option));
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,
                android.R.layout.select_dialog_item, option);
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.DialogStyle);

        builder.setTitle(activity.getString(R.string.select_option));
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                Log.e("Selected Item", String.valueOf(which));

                if (which == 1) {

                    Intent selectImageIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                    selectImageIntent.setType("image/*");
                    selectImageIntent.putExtra("crop", "true");
                    selectImageIntent.putExtra("scale", true);
                    selectImageIntent.putExtra("outputX", 256);
                    selectImageIntent.putExtra("outputY", 256);
                    selectImageIntent.putExtra("aspectX", 1);
                    selectImageIntent.putExtra("aspectY", 1);
                    selectImageIntent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
                    selectImageIntent.putExtra(MediaStore.EXTRA_OUTPUT, getTempUri());
                    activity.startActivityForResult(selectImageIntent, requestcodegallery);
                }

            }
        });
        builder.show();*//*

    }*/
    private void dispatchTakePictureIntent(final int requestcode) {

      /*  String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "Camera_" + timeStamp + "_";

        tempOutputFile = new File(getActivity().getExternalCacheDir(), imageFileName + ".jpeg");

        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);*/
        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "Camera_" + timeStamp + "_";
            tempOutputFile = new File(getActivity().getExternalCacheDir(), imageFileName + ".jpeg");
            Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);


            captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(getActivity(),
                    BuildConfig.APPLICATION_ID + ".provider",
                    tempOutputFile));
            startActivityForResult(captureIntent, requestcode);
          /*  if(Build.VERSION.SDK_INT >= 23) {

                captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(getActivity(),
                        BuildConfig.APPLICATION_ID + ".provider",
                        tempOutputFile));
                startActivityForResult(captureIntent, requestcode);
            }else{
                captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, tempOutputFile);
                startActivityForResult(captureIntent, requestcode);
            }*/


        } catch (Exception e) {
            Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
        }
    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != getActivity().RESULT_OK) {
            getActivity().finish();
        }

        if (requestCode == RESULT_LOAD_IMAGE1 ) {


            try{
                Uri outputFile;

                if (data != null && (data.getAction() == null || !data.getAction().equals(MediaStore.ACTION_IMAGE_CAPTURE))) {
                    outputFile = data.getData();
                    actualImage = new File(getPath(outputFile));

                } else {

                    actualImage = tempOutputFile;
//                actualImageView.setImageBitmap(BitmapFactory.decodeFile(actualImage.getAbsolutePath(),options));
                    // actualSizeTextView.setData(String.format("Size : %s", getReadableFileSize(actualImage.length())));
                }


                if (actualImage == null) {
                    Toast.makeText(getActivity(),  "Please choose the image", Toast.LENGTH_LONG).show();
                } else {

                    compressedImage = new Compressor.Builder(getActivity())
                            .setMaxWidth(1024)
                            .setMaxHeight(720)
                            .setQuality(100)
                            .setCompressFormat(Bitmap.CompressFormat.JPEG)
                            .setDestinationDirectoryPath(getActivity().getExternalFilesDir(
                                    Environment.DIRECTORY_PICTURES).getAbsolutePath())
                            .build()
                            .compressToFile(actualImage);
                    setPic(requestCode, compressedImage.getAbsolutePath());
                    pictureFile = compressedImage;
                }


                //UpdateUserProfile(compressedImage);
                // uploadimageintoserver(compressedImage);

            }catch(Exception e){
                // Toast.makeText(SignatureActivity.this,"CompressedException:"+e.getMessage(),Toast.LENGTH_LONG).show();
            }
        }    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
        if (cursor == null) return null;
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String s = cursor.getString(column_index);
        cursor.close();
        return s;
    }


    private void setPic(int requestcode, String pathfile) {


        try {

            if (requestcode == 1) {
                picturePath1 = pathfile;
                // settingView.takeImage1.setImageBitmap(BitmapFactory.decodeFile(picturePath1));
                Glide.with(getActivity()).load(picturePath1)
                        .into(view.addPropertyDialog.binding.ivProfile);
            }


        } catch (OutOfMemoryError e) {
            Toast.makeText(getActivity(), "OutOfMemoryException:"+e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private Uri getTempUri() {
        return Uri.fromFile(getTempFile());
    }

    private File getTempFile() {

        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {

            File file = new File(Environment.getExternalStorageDirectory(), "TEMP_PHOTO_FILE");
            try {
                file.createNewFile();
            } catch (IOException e) {

            }

            return file;
        } else {

            return null;
        }
    }
}
