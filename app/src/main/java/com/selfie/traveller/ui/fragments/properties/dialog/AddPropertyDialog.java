package com.selfie.traveller.ui.fragments.properties.dialog;


import android.app.Activity;
import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;

import com.selfie.traveller.R;
import com.selfie.traveller.databinding.AddNewPropertyBinding;

public class AddPropertyDialog {


    Dialog dialog;
    Activity activity;

    public AddNewPropertyBinding binding;


    public AddPropertyDialog(Activity activity) {
        this.activity = activity;
        dialog = new Dialog(activity, R.style.DialogStyle);
        binding = DataBindingUtil.inflate(LayoutInflater.from(activity),R.layout.add_new_property,null,false);
        dialog.setContentView(binding.getRoot());





            }


    public void showDialog(boolean doShow) {
        if (doShow){
           // dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.show();
        }
        else
            dialog.dismiss();
    }


}
