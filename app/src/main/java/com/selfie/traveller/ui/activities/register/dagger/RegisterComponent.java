package com.selfie.traveller.ui.activities.register.dagger;


import com.selfie.traveller.application.dagger.AppComponent;
import com.selfie.traveller.ui.activities.register.RegisterActivity;

import dagger.Component;

@RegisterScope
@Component(modules = RegisterModule.class, dependencies = AppComponent.class)
public interface RegisterComponent {
    void inject(RegisterActivity registerActivity);
}
