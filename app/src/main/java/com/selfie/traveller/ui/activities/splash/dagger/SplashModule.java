package com.selfie.traveller.ui.activities.splash.dagger;


import android.app.Activity;


import com.selfie.traveller.ext.storage.PreferencesManager;
import com.selfie.traveller.ui.activities.splash.mvp.SplashModel;
import com.selfie.traveller.ui.activities.splash.mvp.SplashPresenter;
import com.selfie.traveller.ui.activities.splash.mvp.SplashView;


import dagger.Module;
import dagger.Provides;

@Module
public class SplashModule {

    private final Activity activity;

    public SplashModule(Activity activity) {
        this.activity = activity;
    }

    @SplashScope
    @Provides
    public SplashView splashView()
    {
        return new SplashView(activity);
    }


    @SplashScope
    @Provides
    public SplashModel splashModel(PreferencesManager preferencesManager)
    {
        return new SplashModel(activity,preferencesManager);
    }

    @SplashScope
    @Provides
    public SplashPresenter splashPresenter(SplashView splashView, SplashModel splashModel, PreferencesManager preferencesManager)
    {
        return new SplashPresenter(splashView,splashModel, preferencesManager);
    }
}
