package com.selfie.traveller.ui.fragments.properties;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;


@AutoValue
public abstract class AddpropertyParams implements Parcelable {


    @SerializedName("photos[]")
    public abstract String photos();

    @SerializedName("title")
    public abstract String titlep();

    @SerializedName("price")
    public abstract String price();

    @SerializedName("rooms")
    public abstract String rooms();

    @SerializedName("baths")
    public abstract String baths();

    @SerializedName("guests")
    public abstract String guests();

    @SerializedName("desc")
    public abstract String desc();

    @SerializedName("amenities[]")
    public abstract String amenities();

    @SerializedName("lat")
    public abstract String lat();

    @SerializedName("lon")
    public abstract String lon();


    @AutoValue.Builder
    public abstract static class Builder {

        public abstract Builder photos(String photos);

        public abstract Builder titlep(String title);


        public abstract Builder price(String price);


        public abstract Builder rooms(String rooms);

        public abstract Builder baths(String baths);

        public abstract Builder guests(String guests);

        public abstract Builder desc(String desc);

        public abstract Builder amenities(String amenities);

        public abstract Builder lat(String lat);

        public abstract Builder lon(String lon);

        public abstract AddpropertyParams build();
    }

    public static Builder builder() {
        return new $$AutoValue_AddpropertyParams.Builder();
    }


    public static TypeAdapter<AddpropertyParams> typeAdapter(Gson gson) {
        return new $AutoValue_AddpropertyParams.GsonTypeAdapter(gson);
    }

}
