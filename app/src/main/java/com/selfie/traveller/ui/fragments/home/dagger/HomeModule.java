package com.selfie.traveller.ui.fragments.home.dagger;


import android.support.v7.app.AppCompatActivity;


import com.selfie.traveller.application.AppNetwork;
import com.selfie.traveller.ext.storage.PreferencesManager;
import com.selfie.traveller.ui.fragments.home.mvp.HomeModel;
import com.selfie.traveller.ui.fragments.home.mvp.HomePresenter;
import com.selfie.traveller.ui.fragments.home.mvp.HomeView;

import dagger.Module;
import dagger.Provides;

@Module
public class HomeModule {

    private final AppCompatActivity activity;

    public HomeModule(AppCompatActivity activity) {
        this.activity = activity;
    }

    @HomeScope
    @Provides
    public HomeView homeView() {
        return new HomeView(activity);
    }

    @HomeScope
    @Provides
    public HomeModel homeModel(AppNetwork appNetwork, PreferencesManager preferencesManager) {
        return new HomeModel(activity, appNetwork, preferencesManager);
    }

    @HomeScope
    @Provides
    public HomePresenter homePresenter(HomeView homeView, HomeModel homeModel) {
        return new HomePresenter(homeView, homeModel);
    }





}
