package com.selfie.traveller.ui.fragments.home.dagger;




import com.selfie.traveller.application.dagger.AppComponent;
import com.selfie.traveller.ui.fragments.home.HomeFragment;

import dagger.Component;

@HomeScope
@Component(modules = HomeModule.class, dependencies = AppComponent.class)
public interface HomeComponent {

    void inject(HomeFragment homeFragment);
}
