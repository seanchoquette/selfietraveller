package com.selfie.traveller.ui.activities.homepage.dagger;

import android.support.v7.app.AppCompatActivity;

import com.selfie.traveller.application.AppNetwork;
import com.selfie.traveller.ext.storage.PreferencesManager;
import com.selfie.traveller.ui.activities.homepage.mvp.HomePageModel;
import com.selfie.traveller.ui.activities.homepage.mvp.HomePagePresenter;
import com.selfie.traveller.ui.activities.homepage.mvp.HomePageView;
import com.selfie.traveller.ui.fragments.home.HomeFragment;
import com.selfie.traveller.ui.fragments.properties.PropertyListFragment;

import dagger.Module;
import dagger.Provides;


@Module
public class HomePageModule {

    private final AppCompatActivity activity;

    public HomePageModule(AppCompatActivity activity) {
        this.activity = activity;
    }


    @HomePageScope
    @Provides
    public HomePageView homePageView(HomeFragment homeFragment,
                                     PropertyListFragment propertyListFragment,
                                     PreferencesManager preferencesManager) {

        return new HomePageView(activity,
                homeFragment,
                propertyListFragment,
                preferencesManager);
    }


    @HomePageScope
    @Provides
    public HomePageModel homePageModel(AppNetwork appNetwork, PreferencesManager preferencesManager) {
        return new HomePageModel(activity, appNetwork, preferencesManager);
    }


    @HomePageScope
    @Provides
    public HomePagePresenter homePagePresenter(HomePageView homePageView, HomePageModel homePageModel, PreferencesManager preferencesManager) {
        return new HomePagePresenter(homePageView, homePageModel, preferencesManager);
    }


    @HomePageScope
    @Provides
    public HomeFragment homeFragment() {
        return new HomeFragment();
    }


    @HomePageScope
    @Provides
    public PropertyListFragment propertyListFragment() {
        return new PropertyListFragment();
    }



}
