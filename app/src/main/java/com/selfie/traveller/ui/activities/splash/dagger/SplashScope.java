package com.selfie.traveller.ui.activities.splash.dagger;


import javax.inject.Scope;

@Scope
public @interface SplashScope {
}
