package com.selfie.traveller.ui.fragments.properties.mvp;

import android.annotation.SuppressLint;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.selfie.traveller.R;
import com.selfie.traveller.ext.storage.PreferencesManager;
import com.selfie.traveller.ui.fragments.properties.adapter.PropertyAdapterN;
import com.selfie.traveller.ui.fragments.properties.dialog.AddPropertyDialog;
import com.selfie.traveller.ui.fragments.properties.propertiesResponse.Datum;

import java.util.List;

import butterknife.ButterKnife;


@SuppressLint("ViewConstructor")
public class PropertyView extends FrameLayout {

    public AppCompatActivity activity;
    RecyclerView recyclerView;
    PreferencesManager preferencesManager;
    PropertyAdapterN adapterN;
    LinearLayoutManager layoutManager;
    TextView noContentMsg;
    public Button btnAddproperty;
    //FragmentPropertyBinding binding;
    public AddPropertyDialog addPropertyDialog;

    public PropertyView(@NonNull AppCompatActivity activity, PreferencesManager preferencesManager, PropertyAdapterN adapterN,AddPropertyDialog addPropertyDialog) {
        super(activity);
        this.activity = activity;
        this.adapterN = adapterN;
        this.addPropertyDialog = addPropertyDialog;
        inflate(activity, R.layout.fragment_property, this);
        recyclerView = findViewById(R.id.recycler_view);
        btnAddproperty = findViewById(R.id.btn_add_new_property);
        //binding = DataBindingUtil.inflate(LayoutInflater.from(activity),R.layout.fragment_property,null,false);
        //activity.setContentView(binding.getRoot());
        this.preferencesManager = preferencesManager;

        layoutManager = new LinearLayoutManager(activity);
        int firstVisiblePosition = layoutManager.findFirstVisibleItemPosition();
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapterN);
        layoutManager.scrollToPositionWithOffset(firstVisiblePosition, 0);


        btnAddproperty.setOnClickListener(v->{
            addPropertyDialog.showDialog(true);

        });

    }


    public void setNewsList(List<Datum> response) {
        adapterN.showList(response, activity);

    }


/*
    //detail click observable
    public Observable<DatumN> getDetailClickObservable() {
        return adapterN.getDetailClickedObservable();
    }


    public void StartDetail(String id) {

        Intent intent = new Intent(activity, NewsDtailActivity.class);
        intent.putExtra("VIDEOID", id);
        activity.startActivity(intent);
    }*/


    public void showLoading(boolean isLoading) {
        if (isLoading) {
            (findViewById(R.id.loading)).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.loading).setVisibility(View.GONE);
        }

    }

    public void showLoadingg(boolean isLoading) {
        if (isLoading) {
            findViewById(R.id.loadingg).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.loadingg).setVisibility(View.GONE);
        }

    }

   /* public void showNoContent(boolean show, String message) {
        if (show) {

            binding.noContentView.setVisibility(View.VISIBLE);
            binding.msg.setText(message);

        } else {
            binding.noContentView.setVisibility(View.GONE);
        }
    }*/

    public void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }


    public LinearLayoutManager layoutManager() {
        return layoutManager;
    }

}
