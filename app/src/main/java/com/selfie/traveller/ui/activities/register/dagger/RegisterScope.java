package com.selfie.traveller.ui.activities.register.dagger;

import javax.inject.Scope;

@Scope
public @interface RegisterScope {
}
