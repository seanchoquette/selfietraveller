package com.selfie.traveller.ui.activities.register.mvp;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.selfie.traveller.R;
import com.selfie.traveller.databinding.RegisterLayoutOneBinding;
import com.selfie.traveller.ext.storage.PreferencesManager;
import com.selfie.traveller.ui.activities.register.dialog.RegisterFifthDialog;
import com.selfie.traveller.ui.activities.register.dialog.RegisterFourthDialog;
import com.selfie.traveller.ui.activities.register.dialog.RegisterSecondDialog;
import com.selfie.traveller.ui.activities.register.dialog.RegisterThirdDialog;

public class RegisterView extends FrameLayout {



    PreferencesManager sharedPreferenceManager;
    Activity mActivity;
    RegisterSecondDialog registerSecondDialog;
    RegisterThirdDialog registerThirdDialog;
    RegisterFourthDialog registerFourthDialog;
    RegisterFifthDialog registerFifthDialog;

public RegisterLayoutOneBinding binding;
    public RegisterView(@NonNull AppCompatActivity activity, RegisterSecondDialog registerSecondDialog, RegisterThirdDialog registerThirdDialog, RegisterFourthDialog registerFourthDialog, RegisterFifthDialog registerFifthDialog,PreferencesManager preferencesManager) {
        super(activity);
        this.mActivity = activity;
        this.sharedPreferenceManager = preferencesManager;
        this.registerSecondDialog =registerSecondDialog;
        this.registerThirdDialog =registerThirdDialog;
        this.registerFourthDialog =registerFourthDialog;
        this.registerFifthDialog =registerFifthDialog;
        binding = DataBindingUtil.setContentView(activity, R.layout.register_layout_one);
        binding.btnNext.setOnClickListener(v->{
            if(ValidationOne()){
                registerSecondDialog.showDialog(true);
            }

        });

        registerSecondDialog.binding.btnNext.setOnClickListener(v->{

            if(ValidationSecond()){
                registerThirdDialog.showDialog(true);
            }

        });

        registerSecondDialog.binding.btnPrevious.setOnClickListener(v->{

                registerSecondDialog.showDialog(false);


        });

        registerThirdDialog.binding.btnNext.setOnClickListener(v->{

                registerFourthDialog.showDialog(true);


        });
        registerThirdDialog.binding.btnPrevious.setOnClickListener(v->{

                registerThirdDialog.showDialog(false);

        });

        registerFourthDialog.binding.btnNext.setOnClickListener(v->{

                registerFifthDialog.showDialog(true);

        });

        registerFourthDialog.binding.btnPrevious.setOnClickListener(v->{

                registerFourthDialog.showDialog(false);

        });


    }

    public Boolean ValidationOne(){
        if(binding.etUsername.getText().toString().isEmpty()){
            binding.etUsername.setError("Please enter Username");
            return false;
        }
        if(binding.etEmail.getText().toString().isEmpty()){
            binding.etEmail.setError("Please enter Email");
            return false;
        }
        if(binding.etPassword.getText().toString().isEmpty()){
            binding.etPassword.setError("Please enter Password");
            return false;
        }
        if(!binding.etPassword.getText().toString().equals(binding.etConfirmPassword.getText().toString())){
            binding.etConfirmPassword.setError("Password doestn match");
            return false;
        }

        return true;
    }

    public Boolean ValidationSecond(){
        if(registerSecondDialog.binding.etFirstname.getText().toString().isEmpty()){
            registerSecondDialog.binding.etFirstname.setError("Please enter Username");
            return false;
        }
        if(registerSecondDialog.binding.etLastname.getText().toString().isEmpty()){
            registerSecondDialog.binding.etLastname.setError("Please enter Email");
            return false;
        }



        return true;
    }



    public void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }


    public void showLoading(boolean isLoading) {
        if (isLoading) {
          // binding.include.findViewById(R.id.loading).setVisibility(View.VISIBLE);
        } else {
           // binding.include.findViewById(R.id.loading).setVisibility(View.GONE);
        }

        }

}




