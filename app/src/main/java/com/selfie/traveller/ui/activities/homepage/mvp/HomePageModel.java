package com.selfie.traveller.ui.activities.homepage.mvp;

import android.app.Activity;

import com.selfie.traveller.application.AppNetwork;
import com.selfie.traveller.ext.storage.PreferencesManager;

public class HomePageModel {

    private final Activity activity;
    private final AppNetwork appNetwork;
    private final PreferencesManager preferencesManager;

    public HomePageModel(Activity activity, AppNetwork appNetwork, PreferencesManager preferencesManager) {
        this.activity = activity;
        this.appNetwork = appNetwork;
        this.preferencesManager = preferencesManager;
    }



    public String getData(String key) {
        return preferencesManager.get(key);
    }

    public void saveData(String key, String value) {
        preferencesManager.save(key, value);
    }
}
