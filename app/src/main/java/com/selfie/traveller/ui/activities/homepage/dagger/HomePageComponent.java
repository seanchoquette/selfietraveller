package com.selfie.traveller.ui.activities.homepage.dagger;


import com.selfie.traveller.application.dagger.AppComponent;
import com.selfie.traveller.ui.activities.homepage.HomePageActivity;

import dagger.Component;

@HomePageScope
@Component(modules = HomePageModule.class,dependencies = AppComponent.class)
public interface HomePageComponent {

    void inject(HomePageActivity homePageActivity);

}
