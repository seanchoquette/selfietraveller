package com.selfie.traveller.ui.activities.login.dagger;


import com.selfie.traveller.application.dagger.AppComponent;
import com.selfie.traveller.ui.activities.login.LoginActivity;

import dagger.Component;

@LoginScope
@Component(modules = LoginModule.class, dependencies = AppComponent.class)
public interface LoginComponent {
    void inject(LoginActivity loginActivity);
}
