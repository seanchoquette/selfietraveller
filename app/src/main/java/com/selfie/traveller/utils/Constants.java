package com.selfie.traveller.utils;


public class Constants {


    //Api call strings
    public static final String BASE_URL = "http://selfie.coresoftint.com/public/api/v1/";
    public static final String LOGIN = "login";
    public static final String REGISTER = "host/register";
    public static final String PROPERTYLIST = "host/property/list";
    public static final String PROPERTYADD = "host/property/register";




    public static final String LOG_TAG = "SELFIE_DEBUG";
    public static final String HTTP_DIR_CACHE = "vep";
    public static final int CACHE_SIZE = 10 * 1024 * 1024;
    public static final String PREFERENCE_NAME = "selfie_sh";
    public static final String TOKEN = "token";
    public static final String COOKIE = "cookie";

    //user info

    public static final String EMAIL = "email";
    public static final String FIRSTNAME = "first_name";
    public static final String LASTNAME = "last_name";
    public static final String USERID = "id";

    //profile info

    public static final String PHONE = "phone";
    public static final String ADDRESS = "address";
    public static final String CITY = "city";
    public static final String STATE = "state";
    public static final String ZIP = "zip";
    public static final String COUNTRY = "country";
    public static final String PROFILEIMAGE = "profileimage";




}
